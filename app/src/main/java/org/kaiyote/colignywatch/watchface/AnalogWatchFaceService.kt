package org.kaiyote.colignywatch.watchface

import android.content.*
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.wearable.complications.ComplicationData
import android.support.wearable.complications.rendering.ComplicationDrawable
import android.support.wearable.watchface.CanvasWatchFaceService
import android.support.wearable.watchface.WatchFaceService
import android.support.wearable.watchface.WatchFaceStyle
import android.util.Log
import android.util.SparseArray
import android.view.SurfaceHolder
import org.kaiyote.colignywatch.R
import java.lang.ref.WeakReference
import java.util.*
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin
import org.kaiyote.colignywatch.config.AnalogComplicationConfigRecyclerViewAdapter as ACCRVA

class AnalogWatchFaceService : CanvasWatchFaceService() {
    companion object {
        private const val TAG = "AnalogWatchFace"
        private const val LEFT_COMPLICATION_ID = 100
        private const val RIGHT_COMPLICATION_ID = 101
        private const val BOTTOM_LEFT_COMPLICATION_ID = 102
        private const val BOTTOM_RIGHT_COMPLICATION_ID = 103
        private const val TOP_LEFT_COMPLICATION_ID = 104
        private const val TOP_RIGHT_COMPLICATION_ID = 105
        private const val INTERACTIVE_UPDATE_RATE_MS = 1000

        val COMPLICATION_IDS = arrayOf(
                LEFT_COMPLICATION_ID, RIGHT_COMPLICATION_ID, BOTTOM_LEFT_COMPLICATION_ID,
                BOTTOM_RIGHT_COMPLICATION_ID, TOP_LEFT_COMPLICATION_ID, TOP_RIGHT_COMPLICATION_ID
        )

        private val COMPLICATION_SUPPORTED_TYPES = arrayOf(
                ComplicationData.TYPE_RANGED_VALUE,
                ComplicationData.TYPE_ICON,
                ComplicationData.TYPE_SHORT_TEXT,
                ComplicationData.TYPE_SMALL_IMAGE
        )

        fun getComplicationId(complicationLocation: ACCRVA.ComplicationLocation) : Int {
            return when (complicationLocation) {
                ACCRVA.ComplicationLocation.BOTTOM_LEFT -> BOTTOM_LEFT_COMPLICATION_ID
                ACCRVA.ComplicationLocation.BOTTOM_RIGHT -> BOTTOM_RIGHT_COMPLICATION_ID
                ACCRVA.ComplicationLocation.LEFT -> LEFT_COMPLICATION_ID
                ACCRVA.ComplicationLocation.RIGHT -> RIGHT_COMPLICATION_ID
                ACCRVA.ComplicationLocation.TOP_LEFT -> TOP_LEFT_COMPLICATION_ID
                ACCRVA.ComplicationLocation.TOP_RIGHT -> TOP_RIGHT_COMPLICATION_ID
            }
        }

        fun getSupportedComplicationTypes() : Array<Int> = COMPLICATION_SUPPORTED_TYPES

        private const val MSG_UPDATE_TIME = 0
        private const val HOUR_STROKE_WIDTH = 5f
        private const val MINUTE_STROKE_WIDTH = 3f
        private const val SECOND_TICK_STROKE_WIDTH = 2f
        private const val CENTER_GAP_AND_CIRCLE_RADIUS = 4f
        private const val SHADOW_RADIUS = 6f
    }

    private class EngineHandler(reference: Engine) : Handler() {
        private val mWeakReference: WeakReference<Engine> = WeakReference(reference)

        override fun handleMessage(msg: Message) {
            val engine = mWeakReference.get()
            if (engine != null) {
                when (msg.what) {
                    MSG_UPDATE_TIME -> engine.handleUpdateTimeMessage()
                }
            }
        }
    }

    override fun onCreateEngine() : Engine {
        return Engine()
    }

    inner class Engine : CanvasWatchFaceService.Engine() {
        private lateinit var calendar: Calendar
        private var registeredTimeZoneReceiver = false
        private var muteMode = false

        private var centerX = 0f
        private var centerY = 0f
        private var secondHandLength = 0f
        private var minuteHandLength = 0f
        private var hourHandLength = 0f

        private var watchHandAndComplicationsColor = 0
        private var watchHandHighlightColor = 0
        private var watchHandShadowColor = 0
        private var backgroundColor = 0

        private lateinit var hourPaint: Paint
        private lateinit var minutePaint: Paint
        private lateinit var secondAndHighlightPaint: Paint
        private lateinit var tickAndCirclePaint: Paint
        private lateinit var backgroundPaint: Paint

        private lateinit var activeComplicationData: SparseArray<ComplicationData>
        private lateinit var complicationDrawables: SparseArray<ComplicationDrawable>

        private var ambient = false
        private var lowBitAmbient = false
        private var burnInProtection = false

        private var unreadNotificationsPreference = false
        private var numberOfUnreadNotifications = 0

        private lateinit var sharedPref: SharedPreferences

        private var timeZoneReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                calendar.timeZone = TimeZone.getDefault()
                invalidate()
            }
        }

        private var updateTimeHandler = EngineHandler(this)

        override fun onCreate(holder: SurfaceHolder) {
            Log.d(TAG, "onCreate")

            super.onCreate(holder)

            val context = applicationContext
            sharedPref = context.getSharedPreferences(
                    getString(R.string.analog_complication_preference_file_key),
                    Context.MODE_PRIVATE)

            calendar = Calendar.getInstance()

            setWatchFaceStyle(
                    WatchFaceStyle.Builder(this@AnalogWatchFaceService)
                            .setAcceptsTapEvents(true)
                            .setHideNotificationIndicator(true)
                            .build())

            loadSavedPreferences(context)
            initializeComplicationsAndBackground(context)
            initializeWatchFace()
        }

        private fun loadSavedPreferences(context: Context) {
            val backgroundColorResourceName = context.getString(R.string.saved_background_color)
            backgroundColor = sharedPref.getInt(backgroundColorResourceName, Color.BLACK)

            val markerColorResourceName = context.getString(R.string.saved_marker_color)
            watchHandHighlightColor = sharedPref.getInt(markerColorResourceName, Color.RED)

            if (backgroundColor == Color.WHITE) {
                watchHandAndComplicationsColor = Color.BLACK
                watchHandShadowColor = Color.WHITE
            } else {
                watchHandAndComplicationsColor = Color.WHITE
                watchHandShadowColor = Color.BLACK
            }

            val unreadNotificationPreferenceResourceName = context.getString(R.string.saved_unread_notifications_pref)
            unreadNotificationsPreference = sharedPref.getBoolean(unreadNotificationPreferenceResourceName, true)
        }

        private fun initializeComplicationsAndBackground(context: Context) {
            Log.d(TAG, "initializeComplications()")
            backgroundPaint = Paint().apply {
                color = backgroundColor
            }
            activeComplicationData = SparseArray(COMPLICATION_IDS.size)

            val bottomLeftComplicationDrawable = ComplicationDrawable(context)
            val bottomRightComplicationDrawable = ComplicationDrawable(context)
            val leftComplicationDrawable = ComplicationDrawable(context)
            val rightComplicationDrawable = ComplicationDrawable(context)
            val topLeftComplicationDrawable = ComplicationDrawable(context)
            val topRightComplicationDrawable = ComplicationDrawable(context)

            complicationDrawables = SparseArray(COMPLICATION_IDS.size)
            complicationDrawables.put(LEFT_COMPLICATION_ID, leftComplicationDrawable)
            complicationDrawables.put(RIGHT_COMPLICATION_ID, rightComplicationDrawable)
            complicationDrawables.put(BOTTOM_LEFT_COMPLICATION_ID, bottomLeftComplicationDrawable)
            complicationDrawables.put(BOTTOM_RIGHT_COMPLICATION_ID, bottomRightComplicationDrawable)
            complicationDrawables.put(TOP_LEFT_COMPLICATION_ID, topLeftComplicationDrawable)
            complicationDrawables.put(TOP_RIGHT_COMPLICATION_ID, topRightComplicationDrawable)

            setComplicationActiveAndAmbientColors(watchHandHighlightColor)
            setActiveComplications(*COMPLICATION_IDS.toIntArray())
        }

        private fun initializeWatchFace() {
            hourPaint = Paint().apply {
                color = watchHandAndComplicationsColor
                strokeWidth = HOUR_STROKE_WIDTH
                isAntiAlias = true
                strokeCap = Paint.Cap.ROUND
                setShadowLayer(SHADOW_RADIUS, 0f, 0f, watchHandShadowColor)
            }

            minutePaint = Paint().apply {
                color = watchHandAndComplicationsColor
                strokeWidth = MINUTE_STROKE_WIDTH
                isAntiAlias = true
                strokeCap = Paint.Cap.ROUND
                setShadowLayer(SHADOW_RADIUS, 0f, 0f, watchHandShadowColor)
            }

            secondAndHighlightPaint = Paint().apply {
                color = watchHandHighlightColor
                strokeWidth = SECOND_TICK_STROKE_WIDTH
                isAntiAlias = true
                strokeCap = Paint.Cap.ROUND
                setShadowLayer(SHADOW_RADIUS, 0f, 0f, watchHandShadowColor)
            }

            tickAndCirclePaint = Paint().apply {
                color = watchHandAndComplicationsColor
                strokeWidth = SECOND_TICK_STROKE_WIDTH
                isAntiAlias = true
                style = Paint.Style.STROKE
                setShadowLayer(SHADOW_RADIUS, 0f, 0f, watchHandShadowColor)
            }
        }

        private fun setComplicationActiveAndAmbientColors(primaryComplicationColor: Int) {
            for (i in 0 until COMPLICATION_IDS.size) {
                complicationDrawables.get(COMPLICATION_IDS[i]).apply {
                    setBorderColorActive(primaryComplicationColor)
                    setRangedValuePrimaryColorActive(primaryComplicationColor)
                    setBorderColorAmbient(Color.WHITE)
                    setRangedValuePrimaryColorAmbient(Color.WHITE)
                }
            }
        }

        override fun onDestroy() {
            super.onDestroy()
            updateTimeHandler.removeMessages(MSG_UPDATE_TIME)
        }

        override fun onPropertiesChanged(properties: Bundle) {
            super.onPropertiesChanged(properties)
            Log.d(TAG, "onPropertiesChanged: low-bit-ambient = $lowBitAmbient")

            lowBitAmbient = properties.getBoolean(WatchFaceService.PROPERTY_LOW_BIT_AMBIENT, false)
            burnInProtection = properties.getBoolean(WatchFaceService.PROPERTY_BURN_IN_PROTECTION, false)

            for (i in 0 until COMPLICATION_IDS.size) {
                complicationDrawables.get(COMPLICATION_IDS[i]).apply {
                    setLowBitAmbient(lowBitAmbient)
                    setBurnInProtection(burnInProtection)
                }
            }
        }

        override fun onComplicationDataUpdate(watchFaceComplicationId: Int, data: ComplicationData) {
            Log.d(TAG, "onComplicationUpdate() id: $watchFaceComplicationId")

            activeComplicationData.put(watchFaceComplicationId, data)
            complicationDrawables.get(watchFaceComplicationId).setComplicationData(data)
            invalidate()
        }

        override fun onTapCommand(tapType: Int, x: Int, y: Int, eventTime: Long) {
            Log.d(TAG, "OnTapCommand()")
            when (tapType) {
                WatchFaceService.TAP_TYPE_TAP ->
                    for (i in 0 until COMPLICATION_IDS.size) {
                        if (complicationDrawables.get(COMPLICATION_IDS[i]).onTap(x, y)) return
                    }
            }
        }

        override fun onTimeTick() {
            super.onTimeTick()
            invalidate()
        }

        override fun onAmbientModeChanged(inAmbientMode: Boolean) {
            super.onAmbientModeChanged(inAmbientMode)
            Log.d(TAG, "onAmbientModeChanged: $inAmbientMode")

            ambient = inAmbientMode
            updateWatchPaintStyles()

            for (i in 0 until COMPLICATION_IDS.size) {
                complicationDrawables.get(COMPLICATION_IDS[i]).setInAmbientMode(ambient)
            }

            updateTimer()
        }

        private fun updateWatchPaintStyles() {
            if (ambient) {
                backgroundPaint.color = Color.BLACK
                for (paint in arrayOf(hourPaint, minutePaint, secondAndHighlightPaint, tickAndCirclePaint)) {
                    paint.color = Color.WHITE
                    paint.isAntiAlias = false
                    paint.clearShadowLayer()
                }
            } else {
                backgroundPaint.color = backgroundColor
                for (paint in arrayOf(hourPaint, minutePaint, secondAndHighlightPaint, tickAndCirclePaint)) {
                    paint.color = if (paint == tickAndCirclePaint) watchHandHighlightColor else watchHandAndComplicationsColor
                    paint.isAntiAlias = true
                    paint.setShadowLayer(SHADOW_RADIUS, 0f, 0f, watchHandShadowColor)
                }
            }
        }

        override fun onInterruptionFilterChanged(interruptionFilter: Int) {
            super.onInterruptionFilterChanged(interruptionFilter)
            val inMuteMode = interruptionFilter == WatchFaceService.INTERRUPTION_FILTER_NONE

            if (muteMode != inMuteMode) {
                muteMode = inMuteMode
                hourPaint.alpha = if (inMuteMode) 100 else 255
                minutePaint.alpha = if (inMuteMode) 100 else 255
                secondAndHighlightPaint.alpha = if (inMuteMode) 80 else 255
                invalidate()
            }
        }

        override fun onSurfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            super.onSurfaceChanged(holder, format, width, height)

            centerX = width / 2f
            centerY = height / 2f

            secondHandLength = centerX * 0.875f
            minuteHandLength = centerX * 0.75f
            hourHandLength = centerX * 0.5f

            val sizeOfComplication = width / 4
            val midPointOfScreen = width / 2
            val centHorizOffset = sizeOfComplication / 3
            val centVertOffset = (midPointOfScreen * 0.75).toInt()
            val outerHorizOffset = (midPointOfScreen - (sizeOfComplication * 1.08)).toInt()
            val upperVertOffset = centVertOffset - sizeOfComplication
            val lowerVertOffset = centVertOffset + sizeOfComplication

            val leftBounds = Rect(centHorizOffset, centVertOffset, centHorizOffset + sizeOfComplication, centVertOffset + sizeOfComplication)
            complicationDrawables.get(LEFT_COMPLICATION_ID).bounds = leftBounds

            val rightBounds = Rect(width - centHorizOffset - sizeOfComplication, centVertOffset, width - centHorizOffset, centVertOffset + sizeOfComplication)
            complicationDrawables.get(RIGHT_COMPLICATION_ID).bounds = rightBounds

            val topLeftBounds = Rect(outerHorizOffset, upperVertOffset, outerHorizOffset + sizeOfComplication, upperVertOffset + sizeOfComplication)
            complicationDrawables.get(TOP_LEFT_COMPLICATION_ID).bounds = topLeftBounds

            val topRightBounds = Rect(width - outerHorizOffset - sizeOfComplication, upperVertOffset, width - outerHorizOffset, upperVertOffset + sizeOfComplication)
            complicationDrawables.get(TOP_RIGHT_COMPLICATION_ID).bounds = topRightBounds

            val bottomLeftBounds = Rect(outerHorizOffset, lowerVertOffset, outerHorizOffset + sizeOfComplication, lowerVertOffset + sizeOfComplication)
            complicationDrawables.get(BOTTOM_LEFT_COMPLICATION_ID).bounds = bottomLeftBounds

            val bottomRightBounds = Rect(width - outerHorizOffset - sizeOfComplication, lowerVertOffset, width - outerHorizOffset, lowerVertOffset + sizeOfComplication)
            complicationDrawables.get(BOTTOM_RIGHT_COMPLICATION_ID).bounds = bottomRightBounds
        }

        override fun onDraw(canvas: Canvas, bounds: Rect) {
            val now = System.currentTimeMillis()
            calendar.timeInMillis = now

            drawBackground(canvas)
            drawComplications(canvas, now)
            drawUnreadNotificationIcon(canvas)
            drawWatchFace(canvas)
        }

        private fun drawUnreadNotificationIcon(canvas: Canvas) {
            if (unreadNotificationsPreference && numberOfUnreadNotifications > 0) {
                val width = canvas.width
                val height = canvas.height

                canvas.drawCircle(width / 2f, height - 40f, 10f, tickAndCirclePaint)

                if (!ambient)
                    canvas.drawCircle(width / 2f, height - 40f, 4f, secondAndHighlightPaint)
            }
        }

        private fun drawBackground(canvas: Canvas) {
            if (ambient && (lowBitAmbient || burnInProtection)) canvas.drawColor(Color.BLACK)
            else canvas.drawColor(backgroundColor)
        }

        private fun drawComplications(canvas: Canvas, currentTimeMillis: Long) {
            // only draw left and right if in ambient mode (because i said so)
            for (i in 0 until (if (ambient) 2 else COMPLICATION_IDS.size)) {
                complicationDrawables.get(COMPLICATION_IDS[i]).draw(canvas, currentTimeMillis)
            }
        }

        private fun drawWatchFace(canvas: Canvas) {
            drawTicks(canvas)

            val seconds = calendar.get(Calendar.SECOND) + calendar.get(Calendar.MILLISECOND) / 1000f
            val secondsRotation = seconds * 6f
            val minutesRotation = calendar.get(Calendar.MINUTE) * 6f
            val hourHandOffset = calendar.get(Calendar.MINUTE)  / 2f
            val hoursRotation = calendar.get(Calendar.HOUR) * 30 + hourHandOffset

            canvas.save()

            canvas.rotate(hoursRotation, centerX, centerY)
            canvas.drawLine(centerX, centerY - CENTER_GAP_AND_CIRCLE_RADIUS, centerX, centerY - hourHandLength, hourPaint)

            canvas.rotate(minutesRotation - hoursRotation, centerX, centerY)
            canvas.drawLine(centerX, centerY - CENTER_GAP_AND_CIRCLE_RADIUS, centerX, centerY - minuteHandLength, minutePaint)

            if (!ambient) {
                canvas.rotate(secondsRotation - minutesRotation, centerX, centerY)
                canvas.drawLine(centerX, centerY - CENTER_GAP_AND_CIRCLE_RADIUS, centerX, centerY - secondHandLength, secondAndHighlightPaint)
            }

            canvas.drawCircle(centerX, centerY, CENTER_GAP_AND_CIRCLE_RADIUS, tickAndCirclePaint)

            canvas.restore()
        }

        private fun drawTicks(canvas: Canvas) {
            val innerTickRadius = centerX - 10f
            val outerTickRadius = centerX

            for (i in 0 until 12) {
                val tickRot = i * PI * 2 / 12
                val innerX = sin(tickRot).toFloat() * innerTickRadius
                val innerY = -cos(tickRot).toFloat() * innerTickRadius
                val outerX = sin(tickRot).toFloat() * outerTickRadius
                val outerY = -cos(tickRot).toFloat() * outerTickRadius
                canvas.drawLine(centerX + innerX, centerY + innerY, centerX + outerX, centerY + outerY, tickAndCirclePaint)
            }
        }

        override fun onVisibilityChanged(visible: Boolean) {
            super.onVisibilityChanged(visible)

            if (visible) {
                loadSavedPreferences(applicationContext)

                setComplicationActiveAndAmbientColors(watchHandHighlightColor)
                updateWatchPaintStyles()

                registerReceiver()
                calendar.timeZone = TimeZone.getDefault()
                invalidate()
            } else {
                unregisterReceiver()
            }

            updateTimer()
        }

        override fun onUnreadCountChanged(count: Int) {
            Log.d(TAG, "onUnreadCountChanged(): $count")

            if (unreadNotificationsPreference && numberOfUnreadNotifications != count) {
                numberOfUnreadNotifications = count
                invalidate()
            }
        }

        private fun registerReceiver() {
            if (registeredTimeZoneReceiver) return
            registeredTimeZoneReceiver = true
            val filter = IntentFilter(Intent.ACTION_TIMEZONE_CHANGED)
            this@AnalogWatchFaceService.registerReceiver(timeZoneReceiver, filter)
        }

        private fun unregisterReceiver() {
            if (!registeredTimeZoneReceiver) return
            registeredTimeZoneReceiver = false
            this@AnalogWatchFaceService.unregisterReceiver(timeZoneReceiver)
        }

        private fun updateTimer() {
            updateTimeHandler.removeMessages(MSG_UPDATE_TIME)
            if (shouldTimerBeRunning()) updateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
        }

        private fun shouldTimerBeRunning() : Boolean = isVisible && !ambient

        fun handleUpdateTimeMessage() {
            invalidate()
            if (shouldTimerBeRunning()) {
                val timeMs = System.currentTimeMillis()
                val delayMs = INTERACTIVE_UPDATE_RATE_MS - (timeMs % INTERACTIVE_UPDATE_RATE_MS)
                updateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs)
            }
        }
    }
}
