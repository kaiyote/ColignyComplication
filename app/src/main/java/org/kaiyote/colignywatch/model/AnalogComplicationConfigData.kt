package org.kaiyote.colignywatch.model

import android.content.Context
import android.graphics.Color
import org.kaiyote.colignywatch.R
import org.kaiyote.colignywatch.config.AnalogComplicationConfigRecyclerViewAdapter
import org.kaiyote.colignywatch.config.ColorSelectionActivity
import org.kaiyote.colignywatch.watchface.AnalogWatchFaceService

interface ConfigItemType {
    fun getConfigType(): Int
}

class AnalogComplicationConfigData {
    companion object {
        val watchFaceServiceClass = AnalogWatchFaceService::class.java

        // Material Design color options
        val colorOptionsDataSet = arrayOf(
                "#FFFFFF", // white
                "#FFEB3B", // yellow
                "#FFC107", // amber
                "#FF9800", // orange
                "#FF5722", // deep orange
                "#F44336", // red
                "#E91E63", // pink
                "#9C27B0", // purple
                "#673AB7", // deep purple
                "#3F51B5", // indigo
                "#2196F3", // blue
                "#03A9FA", // light blue
                "#00BCD4", // cyan
                "#009688", // teal
                "#4CAF50", // green
                "#8BC34A", // lime green
                "#CDDC39", // lime
                "#607D8B", // blue grey
                "#9E9E9E", // grey
                "#795548", // brown
                "#000000" // black
        ).map { Color.parseColor(it) }

        fun getDataToPopulateAdapter(context: Context): List<ConfigItemType> {
            return arrayOf(
                    PreviewAndComplicationsConfigItem(R.drawable.add_complication),
                    MoreOptionsConfigItem(R.drawable.ic_expand_more_white_18dp),
                    ColorConfigItem(context.getString(R.string.config_marker_color_label), R.drawable.icn_styles, context.getString(R.string.saved_marker_color), ColorSelectionActivity::class.java),
                    ColorConfigItem(context.getString(R.string.config_background_color_label), R.drawable.icn_styles, context.getString(R.string.saved_background_color), ColorSelectionActivity::class.java),
                    UnreadNotificationConfigItem(context.getString(R.string.config_unread_notifications_label), R.drawable.ic_notifications_white_24dp, R.drawable.ic_notifications_off_white_24dp, R.string.saved_unread_notifications_pref)
            ).toList()
        }
    }

    class PreviewAndComplicationsConfigItem(var defaultComplicationResourceId: Int) : ConfigItemType {
        override fun getConfigType(): Int = AnalogComplicationConfigRecyclerViewAdapter.TYPE_PREVIEW_AND_COMPLICATIONS_CONFIG
    }

    class MoreOptionsConfigItem(var iconResourceId: Int) : ConfigItemType {
        override fun getConfigType(): Int = AnalogComplicationConfigRecyclerViewAdapter.TYPE_MORE_OPTIONS
    }

    class ColorConfigItem(var name: String, var iconResourceId: Int, var sharedPrefString: String, var activity: Class<ColorSelectionActivity>) : ConfigItemType {
        override fun getConfigType(): Int = AnalogComplicationConfigRecyclerViewAdapter.TYPE_COLOR_CONFIG
    }

    class UnreadNotificationConfigItem(var name: String, var iconEnabledResourceId: Int, var iconDisabledResourceId: Int, var sharedPrefId: Int) : ConfigItemType {
        override fun getConfigType(): Int = AnalogComplicationConfigRecyclerViewAdapter.TYPE_UNREAD_NOTIFICATION_CONFIG
    }
}
