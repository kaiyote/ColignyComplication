package org.kaiyote.colignywatch.calendar

import android.os.Parcel
import android.os.Parcelable
import java.time.Duration
import java.time.ZoneId
import java.time.ZonedDateTime
import kotlin.math.max

data class ColignyDate(val year: Int, val month: Int, val dayOfMonth: Int, val monthName: String, val dayName: String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString() ?: "",
            parcel.readString() ?: "")

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(year)
        dest?.writeInt(month)
        dest?.writeInt(dayOfMonth)
        dest?.writeString(monthName)
        dest?.writeString(dayName)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<ColignyDate> {
        override fun createFromParcel(parcel: Parcel): ColignyDate {
            return ColignyDate(parcel)
        }

        override fun newArray(size: Int): Array<ColignyDate?> {
            return arrayOfNulls(size)
        }
    }
}

private data class YearAndDay(val yearOfEpoch: Int = 0, val dayOfYear: Int = 0)
private data class MonthAndDay(val monthOfYear: Int = 0, val dayOfMonth: Int = 0)

class ColignyCalendar {
    companion object {
        private const val EXTRA_MONTH = "Ciallos Bis"

        private val MONTH_NAMES = arrayOf("Samoni", "Dumannos", "Rivros", "Anagantios", "Ogroni",
                "Cutios", "Giamoni", "Semivisonna", "Equos", "Elembivos", "Edrinios", "Cantlos")

        private val DAY_NAMES = arrayOf("Dilun", "Dimers", "Dimerker", "Diriau", "Digwener", "Disadorn", "Disol")

        // 30 element array, with the day count for each year out of the 30 year cycle
        private val DAYS_PER_YEAR = intArrayOf(1, 2, 3, 4, 5, 6)
                .flatMap { x: Int -> intArrayOf(384, 354, if (x == 6) 354 else 384, 354, 355).asIterable() }

        // 30 element array, with an array of the days per month for each year of the 30 year cycle
        private val DAYS_PER_MONTH = arrayOfNulls<Int>(30)
                .mapIndexed { index: Int, _ ->
                    var standardYear = intArrayOf(30, 29, 30, 29, 30, 30, 29, 30, 29, 29, 30, 29)
                    if (index % 5 == 0) standardYear = intArrayOf(30) + standardYear
                    else if (index % 5 == 2 && index != 27) standardYear = standardYear.take(6).toIntArray() + 30 + standardYear.drop(6)
                    else if (index % 5 == 4) standardYear[8] = 30
                    standardYear.asList()
                }

        private val EPOCH_START = ZonedDateTime.of(1999, 10, 10, 0, 0, 0, 0, ZoneId.systemDefault())

        private val DAYS_PER_30_YEAR_CYCLE = DAYS_PER_YEAR.sum()

        fun todaysDate(): ColignyDate {
            val today = ZonedDateTime.now(ZoneId.systemDefault())
            return colignyDateForDate(today)
        }

        fun colignyDateForDate(date: ZonedDateTime): ColignyDate {
            val (yearOfEpoch, dayOfYear) = dateToYearAndDayOfEpoch(date)
            val (monthOfYear, dayOfMonth) = dayAndYearOfEpochToMonth(yearOfEpoch, dayOfYear)
            val monthName = monthName(yearOfEpoch, monthOfYear)
            val dayName = DAY_NAMES[date.dayOfWeek.ordinal]

            return ColignyDate(yearOfEpoch, monthOfYear, dayOfMonth, monthName, dayName)
        }

        private fun dateToYearAndDayOfEpoch(date: ZonedDateTime, offset: Int = 0): YearAndDay {
            var daysBetween = Duration.between(EPOCH_START, date).toDays().toInt() - offset

            var year = 1

            for (days in DAYS_PER_YEAR) {
                if (daysBetween < days) break
                year++
                daysBetween -= days
            }

            var intermediateResult = YearAndDay()
            if (daysBetween > 384) intermediateResult = dateToYearAndDayOfEpoch(date, DAYS_PER_30_YEAR_CYCLE)
            val (yearOfEpoch, dayOfYear) = intermediateResult

            return YearAndDay(year + max(yearOfEpoch - 1, 0), if (dayOfYear == 0) daysBetween + 1 else dayOfYear)
        }

        private fun dayAndYearOfEpochToMonth(year: Int, day: Int): MonthAndDay {
            val yearOfCycle = if (year % 30 == 0) 30 else year % 30
            val daysThisYear = DAYS_PER_MONTH[yearOfCycle - 1]

            var fullDays = day
            var month = 1

            for (days in daysThisYear) {
                if (fullDays <= days) break
                month++
                fullDays -= days
            }

            return MonthAndDay(month, fullDays)
        }

        private fun monthName(year: Int, month: Int): String {
            val yearOfCycle = if (year % 30 == 0) 30 else year % 30
            val monthsInYear = DAYS_PER_MONTH[yearOfCycle - 1].size

            var months = MONTH_NAMES

            if (monthsInYear != 12) {
                if (year % 5 == 1) months = arrayOf(EXTRA_MONTH) + months
                else if (year % 5 == 3) months = months.take(6).toTypedArray() + EXTRA_MONTH + months.drop(6)
            }

            return months[month - 1]
        }
    }
}
