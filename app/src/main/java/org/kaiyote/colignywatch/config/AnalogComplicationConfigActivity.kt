package org.kaiyote.colignywatch.config

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.wear.widget.WearableRecyclerView
import android.support.wearable.complications.ComplicationProviderInfo
import android.support.wearable.complications.ProviderChooserIntent
import android.util.Log
import org.kaiyote.colignywatch.R
import org.kaiyote.colignywatch.model.AnalogComplicationConfigData

class AnalogComplicationConfigActivity : Activity() {
    companion object {
        private val TAG = AnalogComplicationConfigActivity::class.java.simpleName
        const val COMPLICATION_CONFIG_REQUEST_CODE = 1001
        const val UPDATE_COLORS_CONFIG_REQUEST_CODE = 1002
    }

    private lateinit var wearableRecyclerView: WearableRecyclerView
    private lateinit var adapter: AnalogComplicationConfigRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_analog_complication_config)

        adapter = AnalogComplicationConfigRecyclerViewAdapter(
                applicationContext,
                AnalogComplicationConfigData.watchFaceServiceClass,
                AnalogComplicationConfigData.getDataToPopulateAdapter(this)
        )

        wearableRecyclerView = findViewById<WearableRecyclerView>(R.id.wearable_recycler_view).apply {
            isEdgeItemsCenteringEnabled = true
            layoutManager = LinearLayoutManager(this@AnalogComplicationConfigActivity)
            setHasFixedSize(true)
            adapter = this@AnalogComplicationConfigActivity.adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == COMPLICATION_CONFIG_REQUEST_CODE && resultCode == RESULT_OK) {
            val complicationProviderInfo = data?.getParcelableExtra<ComplicationProviderInfo>(ProviderChooserIntent.EXTRA_PROVIDER_INFO)
            Log.d(TAG, "Provider: $complicationProviderInfo")

            adapter.updateSelectedComplication(complicationProviderInfo)
        } else if (requestCode == UPDATE_COLORS_CONFIG_REQUEST_CODE && resultCode == RESULT_OK) {
            adapter.updatePreviewColors()
        }
    }
}
