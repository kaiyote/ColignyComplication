package org.kaiyote.colignywatch.config

import android.app.Activity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.wear.widget.WearableRecyclerView
import org.kaiyote.colignywatch.R
import org.kaiyote.colignywatch.model.AnalogComplicationConfigData

class ColorSelectionActivity : Activity() {
    companion object {
        const val EXTRA_SHARED_PREF = "org.kaiyote.colignywatch.config.extra.EXTRA_SHARED_PREF"
    }

    private lateinit var configAppearanceWearableRecyclerView: WearableRecyclerView
    private lateinit var colorSelectionRecyclerViewAdapter: ColorSelectionRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_color_selection_config)

        val sharedPrefString = intent.getStringExtra(EXTRA_SHARED_PREF)

        colorSelectionRecyclerViewAdapter = ColorSelectionRecyclerViewAdapter(sharedPrefString)
        configAppearanceWearableRecyclerView = findViewById<WearableRecyclerView>(R.id.wearable_recycler_view).apply {
            isEdgeItemsCenteringEnabled = true
            layoutManager = LinearLayoutManager(this@ColorSelectionActivity)
            setHasFixedSize(true)
            adapter = colorSelectionRecyclerViewAdapter
        }
    }
}
