package org.kaiyote.colignywatch.config

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.RecyclerView
import android.support.wearable.complications.ComplicationHelperActivity
import android.support.wearable.complications.ComplicationProviderInfo
import android.support.wearable.complications.ProviderInfoRetriever
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Switch
import org.kaiyote.colignywatch.R
import org.kaiyote.colignywatch.model.AnalogComplicationConfigData
import org.kaiyote.colignywatch.model.ConfigItemType
import org.kaiyote.colignywatch.watchface.AnalogWatchFaceService
import java.util.concurrent.Executors

class AnalogComplicationConfigRecyclerViewAdapter(var context: Context, watchFaceServiceClass: Class<AnalogWatchFaceService>, private var settingsDataSet: List<ConfigItemType>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val TAG = "CompConfigAdapter"

        const val TYPE_PREVIEW_AND_COMPLICATIONS_CONFIG = 0
        const val TYPE_MORE_OPTIONS = 1
        const val TYPE_COLOR_CONFIG = 2
        const val TYPE_UNREAD_NOTIFICATION_CONFIG = 3
    }

    private val watchFaceComponentName = ComponentName(context, watchFaceServiceClass)
    private var selectedComplicationId = -1
    private val leftComplicationId = AnalogWatchFaceService.getComplicationId(ComplicationLocation.LEFT)
    private val rightComplicationId = AnalogWatchFaceService.getComplicationId(ComplicationLocation.RIGHT)
    private val topLeftComplicationId = AnalogWatchFaceService.getComplicationId(ComplicationLocation.TOP_LEFT)
    private val topRightComplicationId = AnalogWatchFaceService.getComplicationId(ComplicationLocation.TOP_RIGHT)
    private val bottomLeftComplicationId = AnalogWatchFaceService.getComplicationId(ComplicationLocation.BOTTOM_LEFT)
    private val bottomRightComplicationId = AnalogWatchFaceService.getComplicationId(ComplicationLocation.BOTTOM_RIGHT)

    private val sharedPref = context.getSharedPreferences(context.getString(R.string.analog_complication_preference_file_key), Context.MODE_PRIVATE)
    private val providerInfoRetriever = ProviderInfoRetriever(context, Executors.newCachedThreadPool())
    private var previewAndComplicationsViewHolder: PreviewAndComplicationsViewHolder? = null

    init {
        providerInfoRetriever.init()
    }

    enum class ComplicationLocation {
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        LEFT,
        RIGHT,
        TOP_LEFT,
        TOP_RIGHT
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        Log.d(TAG, "onCreateViewHolder(): viewType: $viewType")

        lateinit var viewHolder: RecyclerView.ViewHolder

        when (viewType) {
            TYPE_PREVIEW_AND_COMPLICATIONS_CONFIG -> {
                previewAndComplicationsViewHolder = PreviewAndComplicationsViewHolder(
                        LayoutInflater.from(parent.context)
                                .inflate(
                                        R.layout.config_list_preview_and_complications_item,
                                        parent,
                                        false))
                viewHolder = previewAndComplicationsViewHolder as PreviewAndComplicationsViewHolder
            }
            TYPE_MORE_OPTIONS ->
                viewHolder = MoreOptionsViewHolder(
                        LayoutInflater.from(parent.context)
                                .inflate(
                                        R.layout.config_list_more_options_item,
                                        parent,
                                        false))
            TYPE_COLOR_CONFIG ->
                viewHolder = ColorPickerViewHolder(
                        LayoutInflater.from(parent.context)
                                .inflate(R.layout.config_list_color_item, parent, false))
            TYPE_UNREAD_NOTIFICATION_CONFIG ->
                viewHolder = UnreadNotificationViewHolder(
                        LayoutInflater.from(parent.context)
                                .inflate(
                                        R.layout.config_list_unread_notif_item,
                                        parent,
                                        false))
        }

        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Log.d(TAG, "Element $position set.")

        val configItemType = settingsDataSet[position]

        when (holder.itemViewType) {
            TYPE_PREVIEW_AND_COMPLICATIONS_CONFIG -> {
                val previewComplicationsViewHolder = holder as PreviewAndComplicationsViewHolder
                val previewAndComplicationsConfigItem = configItemType as AnalogComplicationConfigData.PreviewAndComplicationsConfigItem
                val defaultComplicationResourceId = previewAndComplicationsConfigItem.defaultComplicationResourceId
                previewComplicationsViewHolder.setDefaultComplicationDrawable(defaultComplicationResourceId)
                previewComplicationsViewHolder.initializesColorsAndComplications()
            }
            TYPE_MORE_OPTIONS -> {
                val moreOptionsViewHolder = holder as MoreOptionsViewHolder
                val moreOptionsConfigItem = configItemType as AnalogComplicationConfigData.MoreOptionsConfigItem
                moreOptionsViewHolder.setIcon(moreOptionsConfigItem.iconResourceId)
            }
            TYPE_COLOR_CONFIG -> {
                val colorPickerViewHolder = holder as ColorPickerViewHolder
                val colorConfigItem = configItemType as AnalogComplicationConfigData.ColorConfigItem
                val iconResourceId = colorConfigItem.iconResourceId
                val name = colorConfigItem.name
                val sharedPrefString = colorConfigItem.sharedPrefString
                val activity = colorConfigItem.activity

                colorPickerViewHolder.apply {
                    setIcon(iconResourceId)
                    setName(name)
                    setSharedPrefString(sharedPrefString)
                    setLaunchActivityToSelectColor(activity)
                }
            }
            TYPE_UNREAD_NOTIFICATION_CONFIG -> {
                val unreadViewHolder = holder as UnreadNotificationViewHolder
                val unreadConfigItem = configItemType as AnalogComplicationConfigData.UnreadNotificationConfigItem
                val unreadEnabledIconResourceId = unreadConfigItem.iconEnabledResourceId
                val unreadDisabledIconResourceId = unreadConfigItem.iconDisabledResourceId
                val unreadName = unreadConfigItem.name
                val unreadSharedPrefId = unreadConfigItem.sharedPrefId

                unreadViewHolder.apply {
                    setIcons(unreadEnabledIconResourceId, unreadDisabledIconResourceId)
                    setName(unreadName)
                    setSharedPrefId(unreadSharedPrefId)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val configItemType = settingsDataSet[position]
        return configItemType.getConfigType()
    }

    override fun getItemCount(): Int = settingsDataSet.size

    fun updateSelectedComplication(complicationProviderInfo: ComplicationProviderInfo?) {
        Log.d(TAG, "updateSelectedComplication: $previewAndComplicationsViewHolder")

        if (selectedComplicationId >= 0) {
            previewAndComplicationsViewHolder?.updateComplicationViews(selectedComplicationId, complicationProviderInfo)
        }
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        providerInfoRetriever.release()
    }

    fun updatePreviewColors() {
        Log.d(TAG, "updatePreviewColors(): $previewAndComplicationsViewHolder")

        previewAndComplicationsViewHolder?.updateWatchFaceColors()
    }

    inner class PreviewAndComplicationsViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private val watchFaceArmsAndTicksView = view.findViewById<View>(R.id.watch_face_arms_and_ticks)
        private val watchFaceHighlightPreviewView = view.findViewById<View>(R.id.watch_face_highlight)
        private val watchFaceBackgroundPreviewImageView = view.findViewById<ImageView>(R.id.watch_face_background)

        private val leftComplicationBackground = view.findViewById<ImageView>(R.id.left_complication_background)
        private val leftComplication = view.findViewById<ImageButton>(R.id.left_complication).apply {
            setOnClickListener(this@PreviewAndComplicationsViewHolder)
        }

        private val rightComplicationBackground = view.findViewById<ImageView>(R.id.right_complication_background)
        private val rightComplication = view.findViewById<ImageButton>(R.id.right_complication).apply {
            setOnClickListener(this@PreviewAndComplicationsViewHolder)
        }

        private val topLeftComplicationBackground = view.findViewById<ImageView>(R.id.top_left_complication_background)
        private val topLeftComplication = view.findViewById<ImageButton>(R.id.top_left_complication).apply {
            setOnClickListener(this@PreviewAndComplicationsViewHolder)
        }

        private val topRightComplicationBackground = view.findViewById<ImageView>(R.id.top_right_complication_background)
        private val topRightComplication = view.findViewById<ImageButton>(R.id.top_right_complication).apply {
            setOnClickListener(this@PreviewAndComplicationsViewHolder)
        }

        private val bottomLeftComplicationBackground = view.findViewById<ImageView>(R.id.bottom_left_complication_background)
        private val bottomLeftComplication = view.findViewById<ImageButton>(R.id.bottom_left_complication).apply {
            setOnClickListener(this@PreviewAndComplicationsViewHolder)
        }

        private val bottomRightComplicationBackground = view.findViewById<ImageView>(R.id.bottom_right_complication_background)
        private val bottomRightComplication = view.findViewById<ImageButton>(R.id.bottom_right_complication).apply {
            setOnClickListener(this@PreviewAndComplicationsViewHolder)
        }

        private lateinit var defaultComplicationDrawable: Drawable

        override fun onClick(view: View) {
            when (view) {
                leftComplication -> {
                    Log.d(TAG, "Left Complication click()")
                    val activity = view.context as Activity
                    launchComplicationHelperActivity(activity, ComplicationLocation.LEFT)
                }
                rightComplication -> {
                    Log.d(TAG, "Right Complication click()")
                    val activity = view.context as Activity
                    launchComplicationHelperActivity(activity, ComplicationLocation.RIGHT)
                }
                topLeftComplication -> {
                    Log.d(TAG, "Top Left Complication click()")
                    val activity = view.context as Activity
                    launchComplicationHelperActivity(activity, ComplicationLocation.TOP_LEFT)
                }
                topRightComplication -> {
                    Log.d(TAG, "Top Right Complication click()")
                    val activity = view.context as Activity
                    launchComplicationHelperActivity(activity, ComplicationLocation.TOP_RIGHT)
                }
                bottomLeftComplication -> {
                    Log.d(TAG, "Bottom Left Complication click()")
                    val activity = view.context as Activity
                    launchComplicationHelperActivity(activity, ComplicationLocation.BOTTOM_LEFT)
                }
                bottomRightComplication -> {
                    Log.d(TAG, "Bottom Right Complication click()")
                    val activity = view.context as Activity
                    launchComplicationHelperActivity(activity, ComplicationLocation.BOTTOM_RIGHT)
                }
            }
        }

        fun updateWatchFaceColors() {
            val backgroundSharedPrefString = context.getString(R.string.saved_background_color)
            val currentBackgroundColor = sharedPref.getInt(backgroundSharedPrefString, Color.BLACK)
            val backgroundColorFilter = PorterDuffColorFilter(currentBackgroundColor, PorterDuff.Mode.SRC_ATOP)
            watchFaceBackgroundPreviewImageView.background.colorFilter = backgroundColorFilter

            val highlightSharedPrefString = context.getString(R.string.saved_marker_color)
            val currentHighlightColor = sharedPref.getInt(highlightSharedPrefString, Color.RED)
            val highlightColorFilter = PorterDuffColorFilter(currentHighlightColor, PorterDuff.Mode.SRC_ATOP)
            watchFaceHighlightPreviewView.background.colorFilter = highlightColorFilter
        }

        private fun launchComplicationHelperActivity(currentActivity: Activity, complicationLocation: ComplicationLocation) {
            selectedComplicationId = AnalogWatchFaceService.getComplicationId(complicationLocation)
            if (selectedComplicationId >= 0) {
                val supportedTypes = AnalogWatchFaceService.getSupportedComplicationTypes()
                val watchFace = ComponentName(currentActivity, AnalogWatchFaceService::class.java)
                currentActivity.startActivityForResult(
                        ComplicationHelperActivity.createProviderChooserHelperIntent(
                                currentActivity,
                                watchFace,
                                selectedComplicationId,
                                *supportedTypes.toIntArray()),
                        AnalogComplicationConfigActivity.COMPLICATION_CONFIG_REQUEST_CODE
                )
            } else {
                Log.d(TAG, "Complication not supported by watch face")
            }
        }

        fun setDefaultComplicationDrawable(resourceId: Int) {
            val ticksContext = watchFaceArmsAndTicksView.context

            @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
            defaultComplicationDrawable = ticksContext.getDrawable(resourceId)

            for (comp in arrayOf(leftComplication, rightComplication, topLeftComplication, topRightComplication, bottomLeftComplication, bottomRightComplication)) {
                comp.setImageDrawable(defaultComplicationDrawable)
            }

            for (background in arrayOf(leftComplicationBackground, rightComplicationBackground, topLeftComplicationBackground, topRightComplicationBackground, bottomLeftComplicationBackground, bottomRightComplicationBackground)) {
                background.visibility = View.INVISIBLE
            }
        }

        fun updateComplicationViews(watchFaceComplicationId: Int, complicationProviderInfo: ComplicationProviderInfo?) {
            Log.d(TAG, "updateComplicationViews(): id: $watchFaceComplicationId")
            Log.d(TAG, "\tinfo: $complicationProviderInfo")

            when (watchFaceComplicationId) {
                leftComplicationId -> updateComplicationView(complicationProviderInfo, leftComplication, leftComplicationBackground)
                rightComplicationId -> updateComplicationView(complicationProviderInfo, rightComplication, rightComplicationBackground)
                topLeftComplicationId -> updateComplicationView(complicationProviderInfo, topLeftComplication, topLeftComplicationBackground)
                bottomLeftComplicationId -> updateComplicationView(complicationProviderInfo, bottomLeftComplication, bottomLeftComplicationBackground)
                topRightComplicationId -> updateComplicationView(complicationProviderInfo, topRightComplication, topRightComplicationBackground)
                bottomRightComplicationId -> updateComplicationView(complicationProviderInfo, bottomRightComplication, bottomRightComplicationBackground)
            }
        }

        private fun updateComplicationView(complicationProviderInfo: ComplicationProviderInfo?, button: ImageButton, background: ImageView) {
            if (complicationProviderInfo != null) {
                button.setImageIcon(complicationProviderInfo.providerIcon)
                button.contentDescription = context.getString(R.string.edit_complication, "${complicationProviderInfo.appName} ${complicationProviderInfo.providerName}")
                background.visibility = View.VISIBLE
            } else {
                button.setImageDrawable(defaultComplicationDrawable)
                button.contentDescription = context.getString(R.string.add_complication)
                background.visibility = View.INVISIBLE
            }
        }

        fun initializesColorsAndComplications() {
            updateWatchFaceColors()

            val complicationIds = AnalogWatchFaceService.COMPLICATION_IDS
            providerInfoRetriever.retrieveProviderInfo(object : ProviderInfoRetriever.OnProviderInfoReceivedCallback() {
                override fun onProviderInfoReceived(watchFaceComplicationId: Int, info: ComplicationProviderInfo?) {
                    Log.d(TAG, "onProviderInfoReceived: $info")
                    updateComplicationViews(watchFaceComplicationId, info)
                }
            }, watchFaceComponentName, *complicationIds.toIntArray())
        }
    }

    inner class MoreOptionsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val moreOptionsImageView = view.findViewById<ImageView>(R.id.more_options_image_view)

        fun setIcon(resourceId: Int) {
            val context = moreOptionsImageView.context
            moreOptionsImageView.setImageDrawable(context.getDrawable(resourceId))
        }
    }

    inner class ColorPickerViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private lateinit var sharedPrefResourceString: String
        private var launchActivityToSelectColor: Class<ColorSelectionActivity>? = null
        private val appearanceButton = view.findViewById<Button>(R.id.color_picker_button).apply {
            setOnClickListener(this@ColorPickerViewHolder)
        }

        fun setName(name: String) {
            appearanceButton.text = name
        }

        fun setIcon(resourceId: Int) {
            val context = appearanceButton.context
            appearanceButton.setCompoundDrawablesRelativeWithIntrinsicBounds(context.getDrawable(resourceId), null, null, null)
        }

        fun setSharedPrefString(sharedPrefString: String) {
            sharedPrefResourceString = sharedPrefString
        }

        fun setLaunchActivityToSelectColor(activity: Class<ColorSelectionActivity>) {
            launchActivityToSelectColor = activity
        }

        override fun onClick(view: View) {
            val position = adapterPosition
            Log.d(TAG, "Complication onClick() position: $position")

            if (launchActivityToSelectColor != null) {
                val launchIntent = Intent(view.context, launchActivityToSelectColor).apply {
                    putExtra(ColorSelectionActivity.EXTRA_SHARED_PREF, sharedPrefResourceString)
                }

                val activity = view.context as Activity
                activity.startActivityForResult(launchIntent, AnalogComplicationConfigActivity.UPDATE_COLORS_CONFIG_REQUEST_CODE)
            }
        }
    }

    inner class UnreadNotificationViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private var enabledIconResourceId = 0
        private var disabledIconResourceId = 0
        private var sharedPrefResourceId = 0
        private val unreadNotificationSwitch = view.findViewById<Switch>(R.id.unread_notification_switch).apply {
            setOnClickListener(this@UnreadNotificationViewHolder)
        }

        fun setName(name: String) {
            unreadNotificationSwitch.text = name
        }

        fun setIcons(enabledIconResource: Int, disabledIconResource: Int) {
            enabledIconResourceId = enabledIconResource
            disabledIconResourceId = disabledIconResource
            val context = unreadNotificationSwitch.context
            unreadNotificationSwitch.setCompoundDrawablesRelativeWithIntrinsicBounds(context.getDrawable(enabledIconResourceId), null, null, null)
        }

        fun setSharedPrefId(sharedPrefId: Int) {
            sharedPrefResourceId = sharedPrefId

            val context = unreadNotificationSwitch.context
            val sharedPreferenceString = context.getString(sharedPrefResourceId)
            val currentState = sharedPref.getBoolean(sharedPreferenceString, true)
            updateIcon(context, currentState)
        }

        private fun updateIcon(context: Context, currentState: Boolean) {
            val currentIconResourceId = if (currentState) enabledIconResourceId else disabledIconResourceId
            unreadNotificationSwitch.isChecked = currentState
            unreadNotificationSwitch.setCompoundDrawablesRelativeWithIntrinsicBounds(context.getDrawable(currentIconResourceId), null, null, null)
        }

        override fun onClick(view: View) {
            val position = adapterPosition
            Log.d(TAG, "Complication onClick() position: $position")

            val context = view.context
            val sharedPreferenceString = context.getString(sharedPrefResourceId)

            val newState = !sharedPref.getBoolean(sharedPreferenceString, true)

            sharedPref.edit().apply {
                putBoolean(sharedPreferenceString, newState)
                apply()
            }

            updateIcon(context, newState)
        }
    }
}
