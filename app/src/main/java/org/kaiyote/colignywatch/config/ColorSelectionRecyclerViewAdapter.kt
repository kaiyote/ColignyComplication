@file:Suppress("DEPRECATION")

package org.kaiyote.colignywatch.config

import android.app.Activity
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.support.wearable.view.CircledImageView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.kaiyote.colignywatch.R
import org.kaiyote.colignywatch.model.AnalogComplicationConfigData.Companion.colorOptionsDataSet

class ColorSelectionRecyclerViewAdapter(var sharedPrefString: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private val TAG = ColorSelectionRecyclerViewAdapter::class.java.simpleName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        Log.d(TAG, "onCreateViewHolder(): viewType: $viewType")

        return ColorViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.color_config_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Log.d(TAG, "Element $position set.")

        val color = colorOptionsDataSet[position]
        (holder as ColorViewHolder).setColor(color)
    }

    override fun getItemCount(): Int = colorOptionsDataSet.size

    inner class ColorViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private var colorCircleImageView = view.findViewById<CircledImageView>(R.id.color).apply {
            setOnClickListener(this@ColorViewHolder)
        }

        fun setColor(color: Int) = colorCircleImageView.setCircleColor(color)

        override fun onClick(view: View) {
            val position = adapterPosition
            val color = colorOptionsDataSet[position]

            Log.d(TAG, "Color: $color onClick() position: $position")

            val activity = view.context as Activity

            if (sharedPrefString.isNotEmpty()){
                val sharedPref = activity.getSharedPreferences(activity.getString(R.string.analog_complication_preference_file_key), Context.MODE_PRIVATE)

                val editor = sharedPref.edit()
                editor.putInt(sharedPrefString, color)
                editor.apply()

                activity.setResult(Activity.RESULT_OK)
            }

            activity.finish()
        }

    }
}
