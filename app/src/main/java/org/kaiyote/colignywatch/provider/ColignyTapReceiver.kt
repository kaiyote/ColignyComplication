package org.kaiyote.colignywatch.provider

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import org.kaiyote.colignywatch.calendar.ColignyDate

class ColignyTapReceiver : BroadcastReceiver() {
    companion object {
        private const val EXTRA_DATE_COMPONENT = "org.kaiyote.colignywatch.provider.action.DATE_COMPONENT"

        fun getPendingIntent(context: Context, complicationId: Int, colignyDate: ColignyDate?): PendingIntent {
            Log.d("COLIGNY_TOASTER", "WIRING PENDING INTENT")
            val intent = Intent(context, ColignyTapReceiver::class.java).apply {
                putExtra(EXTRA_DATE_COMPONENT, colignyDate)
            }

            return PendingIntent.getBroadcast(context, complicationId, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val extras = intent?.extras
        val date = extras?.getParcelable<ColignyDate?>(EXTRA_DATE_COMPONENT)
        Log.d("COLIGNY_TOASTER", "Trying to show a toast: $date")

        val toastText = "${date?.dayName}, ${date?.monthName} ${ordinal(date?.dayOfMonth)}, ${date?.year}"

        Toast.makeText(context, toastText, Toast.LENGTH_SHORT).show()
    }

    private fun ordinal(num: Int?): String {
        val mod100 = num?.rem(100)
        val mod10 = num?.rem(10)
        return if (mod10 == 1 && mod100 != 11) "${num}st"
        else if (mod10 == 2 && mod100 != 12) "${num}nd"
        else if (mod10 == 3 && mod100 != 13) "${num}rd"
        else "${num}th"
    }
}
