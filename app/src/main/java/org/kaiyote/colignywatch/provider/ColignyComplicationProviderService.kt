package org.kaiyote.colignywatch.provider

import android.support.wearable.complications.ComplicationData
import android.support.wearable.complications.ComplicationManager
import android.support.wearable.complications.ComplicationProviderService
import android.support.wearable.complications.ComplicationText
import android.util.Log
import org.kaiyote.colignywatch.calendar.ColignyCalendar
import org.kaiyote.colignywatch.calendar.ColignyDate
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class ColignyComplicationProviderService : ComplicationProviderService() {
    companion object {
        private const val TAG = "ColignyComplicationProvider"
        private var lastUpdatedDateTime = ""
        private lateinit var colignyDate: ColignyDate
    }

    override fun onComplicationUpdate(complicationId: Int, type: Int, manager: ComplicationManager) {
        var complicationData : ComplicationData? = null
        val currentDay = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)

        Log.d(TAG, "Requested update")
        if (currentDay != lastUpdatedDateTime) {
            lastUpdatedDateTime = currentDay
            colignyDate = ColignyCalendar.todaysDate()
        }

        val pendingIntent = ColignyTapReceiver.getPendingIntent(this, complicationId, colignyDate)

        when (type) {
            ComplicationData.TYPE_SHORT_TEXT ->
                complicationData = ComplicationData.Builder(ComplicationData.TYPE_SHORT_TEXT)
                        .setShortText(ComplicationText.plainText(formatDate(colignyDate, type)))
                        .setTapAction(pendingIntent)
                        .build()
            ComplicationData.TYPE_LONG_TEXT ->
                complicationData = ComplicationData.Builder(ComplicationData.TYPE_LONG_TEXT)
                        .setLongText(ComplicationText.plainText(formatDate(colignyDate, type)))
                        .build()
        }

        Log.d(TAG, "sending update")
        if (complicationData != null) manager.updateComplicationData(complicationId, complicationData)
        else manager.noUpdateRequired(complicationId)
    }

    private fun formatDate(date: ColignyDate, type: Int) : String {
        return when (type) {
            ComplicationData.TYPE_SHORT_TEXT ->
                "${date.dayOfMonth} ${date.monthName.substring(0, 4)}"
            ComplicationData.TYPE_LONG_TEXT ->
                "${date.dayName} ${date.dayOfMonth} ${date.monthName} ${date.year}"
            else -> ""
        }
    }
}
