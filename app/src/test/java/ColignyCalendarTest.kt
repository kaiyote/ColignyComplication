import org.junit.Test
import org.junit.Assert.assertEquals
import org.kaiyote.colignywatch.calendar.ColignyCalendar
import java.time.ZonedDateTime

class ColignyCalendarTest {

  @Test
  fun `works for november 7 2018`() {
    val nov7 = ZonedDateTime.parse("2018-11-07T09:55:00-05:00")
    val result = ColignyCalendar.colignyDateForDate(nov7)
    assertEquals(result.monthName, "Cantlos")
    assertEquals(result.dayOfMonth, 29)
    assertEquals(result.month, 12)
    assertEquals(result.year, 19)
  }

  @Test
  fun `works for november 8 2018`() {
    val nov8 = ZonedDateTime.parse("2018-11-08T09:55:00-05:00")
    val result = ColignyCalendar.colignyDateForDate(nov8)
    assertEquals(result.monthName, "Samoni")
    assertEquals(result.dayOfMonth, 1)
    assertEquals(result.month, 1)
    assertEquals(result.year, 20)
  }
}
